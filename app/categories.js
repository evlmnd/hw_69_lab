const express = require("express");
const router = express.Router();

const createRouter = db => {

  router.get('/', (req, res) => {
    db.query('SELECT `id`, `category` FROM `categories`', function (error, results) {
      if (error) throw error;
      res.send(results);
    })
  });

  router.get('/:id', (req, res) => {
    const id = req.params.id;
    db.query('SELECT * FROM `categories` WHERE `id`= "'+ id + '"', (error, results) => {
      if (error) throw error;
      res.send(results);
    })
  });

  router.post('/', (req, res) => {
    const data = req.body;
    if (data.category) {
      db.query('INSERT INTO `categories` (`category`, `description`) VALUES (?, ?)',
        [data.category, data.description],
        (error, results) => {
          if (error) throw error;
          data.id = results.insertId;
          res.send(data);
        }
      )
    } else res.status(400).send('Field "category" can not be blank');
  });

  router.delete('/:id', (req, res) => {
    const id = req.params.id;
    const data = req.body;

    db.query('SELECT * FROM `items` WHERE `category_id`= "'+ id + '"', (error, results) => {
      if (error) throw error;
      if (results.length === 0) {
        db.query('DELETE FROM `categories` WHERE `id`= "'+ id + '"', (error, results) => {
          if (error) throw error;
          data.id = id;
          res.send(data);
        });
        res.send('delete');
      } else res.status(400).send('You can not delete location, it used');
    });


  });

  router.put('/:id', (req, res) => {
    const data = req.body;
    const id = req.params.id;
    if (!data.category) {
      res.status(400).send('Field "Name" can not be blank');
    } else {
      db.query('UPDATE `categories` SET `category` = ?, `description` = ? WHERE `id` = ?', [data.category, data.description, id],
        (error, results) => {
          if (error) throw error;
          data.id = id;
          res.send(data);
        });
    }
  });

  return router;
};

module.exports = createRouter;
